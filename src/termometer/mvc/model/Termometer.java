package termometer.mvc.model;

import java.util.LinkedList;
import termometer.mvc.event.TermometerListener;

public class Termometer {

    float temperature;

    public Termometer() {
        this.temperature = 0;
    }

    public Termometer(float temperature) {
        this.temperature = temperature;
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
        // La notificación del cambio en los valores del modelo se realiza
        // después de modificar el valor de uno de sus atributos.
        notifyEvent();
    }

    /**
     * Para notificar eventos relacionados con la modificación de los atributos
     * de la clase, es cenesario tener una lista que contenga la referencia a 
     * los manejadores de eventos impleemntados.
     */
    private LinkedList<TermometerListener> listeners = new LinkedList();

    public void addTermometerListener(TermometerListener l) {
        this.listeners.add(l);
    }

    private void notifyEvent() {
        for (TermometerListener manejador : listeners) {
            manejador.updateView();
        }
    }
}
