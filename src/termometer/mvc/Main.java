package termometer.mvc;

import javax.swing.JOptionPane;
import termometer.mvc.model.Termometer;
import termometer.mvc.ui.TermometerView01;
import termometer.mvc.ui.TermometerView02;
import termometer.mvc.ui.TermometerView03;

public class Main {

    public static void main(String[] args) {

        Termometer t = new Termometer(20);

        new TermometerView01(t).setVisible(true);
        new TermometerView02(t).setVisible(true);
        new TermometerView03(t).setVisible(true);
    }
    
}
