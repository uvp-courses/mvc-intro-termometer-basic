
package termometer.mvc.event;

/**
 * La interfaz TermometerListener es la interfaz que el maneador de eventos
 * debe impleemntar para ser notifocado de los cambios en el modelo. Esta es 
 * una interfa definida por el programador, dependiendo de las necesidades
 * de la aplicación.
 * @author Antonio J. Vélez Q. <antonio.velez@correounivalle.edu.co>
 */

public interface TermometerListener {
    public void updateView();
}
